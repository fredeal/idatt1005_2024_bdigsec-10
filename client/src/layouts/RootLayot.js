import Nav from "../components/Nav";
import BottomNav from "../components/BottomNav";
import { Outlet } from "react-router-dom";
import { useSelector } from "react-redux";

export default function RootLayout() {
  const user = useSelector((state) => state.auth.user);
  return (
    <div>
        
      {user ? ( //When user is logged in, both top and bottom nav bars are displayed
        <>
          <Nav />
          <Outlet />
          <BottomNav />
        </>
      ) : ( // No nav bars displayed when user is not logged in
        <>
          <Outlet />
        </>
      )}
    </div>
  );
}
