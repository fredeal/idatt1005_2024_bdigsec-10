import React from "react";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";
import homeIcon from "../icons/home-icon.png";
import profileIcon from "../icons/profile-icon.png";

export default function Navbar() {
  const loggedIn = useSelector((state) => state.auth.isLoggedIn);

  return (
    <nav className="flex items-center justify-between bg-green-800 p-6">
      <div className="flex items-center text-white">
        <Link to="/" className="block px-3 py-1 hover:bg-green-600 rounded">
          <img src={homeIcon} alt="home" className="w-8 h-8" />
        </Link>
      </div>
      <div className="text-white flex-grow text-left hidden sm:block">
        <Link
          to="/recipes"
          className="inline-block px-3 py-1 hover:bg-green-600 rounded"
        >
          Recipes
        </Link>
        {loggedIn && (
          <>
            <Link
              to="/favorites"
              className="inline-block px-3 py-1 hover:bg-green-600 rounded"
            >
              Favorites
            </Link>
            <Link
              to="/inventory"
              className="inline-block px-3 py-1 hover:bg-green-600 rounded"
            >
              Inventory
            </Link>
            <Link
              to="/shoppinglist"
              className="inline-block px-3 py-1 hover:bg-green-600 rounded"
            >
              Shopping List
            </Link>
          </>
        )}
      </div>
      <div className="text-white">
        <Link to="/profile" className="inline-block px-3 py-1 hover:bg-green-600 rounded">
          <img src={profileIcon} alt="profile" className="w-8 h-8" />
        </Link>
      </div>
    </nav>
  );
}
