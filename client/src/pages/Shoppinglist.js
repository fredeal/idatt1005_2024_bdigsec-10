import React, { useState, useEffect } from "react";
import axios from "axios";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { Navigate } from "react-router-dom";
import deleteIcon from "../icons/delete-icon.png";
import addIcon from "../icons/add-icon.png";

function ShoppingList() {
  const [inputValue, setInputValue] = useState("");
  const [inputQuantity, setInputQuantity] = useState("");
  const [listItems, setListItems] = useState([]);
  const [ingredients, setIngredients] = useState([]);
  const username = useSelector((state) => state.auth.user);
  const navigate = useNavigate();

  useEffect(() => {
    axios
      .get("http://localhost:8080/ingredients")
      .then((response) => setIngredients(response.data))
      .catch((error) => console.error("Error fetching ingredients:", error));
  }, []);

  useEffect(() => {
    fetchShoppingList(username);
  }, [username]);

  //Adds item to shopping list
  const handleAddItem = (event) => {
    event.preventDefault();
    if (inputValue && inputQuantity) {
      axios
        .post("http://localhost:8080/shoppinglist", {
          username: username,
          ingredientId: inputValue,
          quantity: inputQuantity,
          unit: "pcs",
        })
        .then(() => fetchShoppingList(username))

        .catch((error) => console.error("Error adding item:", error));
    }
  };

  // delete entire shopping list for specific user
  const deleteShoppingList = async () => {
    try {
      await axios.delete(
        `http://localhost:8080/shoppinglist/${username}/delete`
      );
      navigate("/inventory");
    } catch (error) {
      console.error("Error deleting shopping list:", error);
    }
  };

  // deletes single items from shopping list
  const handleDeleteItem = async (listItemId) => {
    try {
      await axios.delete(`http://localhost:8080/shoppinglist/${listItemId}`);

      await fetchShoppingList(username);
    } catch (error) {
      console.error("Error deleting item:", error);
    }
  };

  //Also groups similar shopping list items. ChatGPT was used to generate and troubleshoot.
  const fetchShoppingList = (username) => {
    axios
      .get(`http://localhost:8080/shoppinglist/${username}`)
      .then((response) => {
        const fetchedListItems = response.data;
        const distinctListItems = [];

        fetchedListItems.forEach((item) => {
          const existingItemIndex = distinctListItems.findIndex(
            (existingItem) => existingItem.ingredientId === item.ingredientId
          );

          if (existingItemIndex !== -1) {
            distinctListItems[existingItemIndex].quantity += parseFloat(
              item.quantity
            );
          } else {
            distinctListItems.push({
              ...item,
              quantity: parseFloat(item.quantity),
            });
          }
        });

        setListItems(distinctListItems);
      })
      .catch((error) => console.error("Error fetching shopping list:", error));
  };

  //Adds all items from shopping list to inventory then calls delete shopping list
  const addToInventory = async (listItems) => {
    try {
      for (const listItem of listItems) {
        await axios.post("http://localhost:8080/inventory", {
          username: username,
          ingredientId: listItem.ingredientId,
          quantity: listItem.quantity,
          unit: listItem.unit,
        });
      }
      await deleteShoppingList(); // Delete entire shopping list after all items are added to inventory
    } catch (error) {
      console.error("Error adding item to inventory:", error);
    }
  };

  return (
    <div style={{ paddingBottom: "65px" }} className="container mx-auto mt-8">
      <h2 className="text-2xl font-semibold mb-4 text-center">Shopping List</h2>
      <form onSubmit={handleAddItem} className="flex flex-col">
        <select
          className="w-full border border-gray-400 p-2 rounded-md mb-2 focus:outline-none"
          value={inputValue}
          onChange={(e) => setInputValue(e.target.value)}
        >
          <option value="">Select ingredient</option>
          {ingredients.map((ingredient, index) => (
            <option key={index} value={ingredient.ingredientId}>
              {ingredient.name}
            </option>
          ))}
        </select>
        <input
          className="w-full border border-gray-400 p-2 rounded-md mb-2 focus:outline-none"
          type="number"
          placeholder="Quantity"
          value={inputQuantity}
          onChange={(e) => setInputQuantity(e.target.value)}
        />
        <button
          className="bg-blue-500 text-white p-2 rounded-md mb-2 flex items-center justify-center hover:bg-blue-400"
          type="submit"
        >
          <img
            src={addIcon}
            alt="Add to Inventory button"
            className="h-8 w-8 mr-2"
          />
          <span>Shopping List</span>
        </button>
      </form>
      <ul className="list-disc pl-5 mt-4">
        {listItems.map((ingredient, index) => (
          <li key={index} className="flex items-center justify-between mb-2">
            <span>{`${ingredient.quantity} ${ingredient.name} ${ingredient.unit}`}</span>
            <button
              className="bg-red-500 text-white p-2 rounded-md hover:bg-red-400"
              onClick={() => handleDeleteItem(ingredient.listItemId)}
            >
              <img src={deleteIcon} alt="Delete from inventory button" />
            </button>
          </li>
        ))}
        <button
          className="bg-green-500 text-white p-2 rounded-md flex items-center justify-center hover:bg-green-400"
          onClick={() => addToInventory(listItems)}
        >
          <img
            src={addIcon}
            alt="Add to Inventory button"
            className="h-8 w-8 mr-2"
          />
          <span>Inventory</span>
        </button>
      </ul>
      {!username && <Navigate to="/" replace={true} />}
    </div>
  );
}

export default ShoppingList;
