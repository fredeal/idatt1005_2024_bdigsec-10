import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { logout } from "../store/authSlice";
import { Navigate } from "react-router-dom";

const Profile = () => {
  const user = useSelector((state) => state.auth.user);
  const dispatch = useDispatch(); //logout action

  return (
    <>
      <div
        style={{ paddingBottom: "65px" }}
        className="max-w-lg mx-auto bg-white shadow-md rounded-lg mb-4 p-6"
      >
        <h1 className="text-3xl font-semibold mb-4">Profile</h1>
        {user && <h2 className="text-lg mb-4">Hello, {user}!</h2>}
        <button
          onClick={() => dispatch(logout())}
          className="bg-red-500 text-white py-2 px-4 rounded-lg mb-4"
        >
          Logout
        </button>
        {!user && <Navigate to="/" replace={true} />}
      </div>
      <div className="fixed w-full flex justify-center items-center bg-white p-4">
        <span className="mt-auto">
          All icons by: 
          <a href="https://icons8.com/" target="_blank" rel="noreferrer" className="text-blue-500 underline"> icons8</a>
        </span>
      </div>
    </>
  );
};


export default Profile;
