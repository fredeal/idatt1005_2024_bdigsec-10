/* eslint-disable no-undef */

describe("shoppinglist", () => {
    it("shoppinglist renders and adds basil ingredient to shopping list", () => {
        cy.visit("/");
        cy.get("#username").type("b");
        cy.get("#password").type("b");
        cy.get("button").click();
        cy.get('.flex-grow > [href="/shoppinglist"]').click();
        cy.get('select.w-full').select('Basil');
        cy.get('input.w-full').type('1');
        cy.get('.bg-blue-500').click();
});
});
