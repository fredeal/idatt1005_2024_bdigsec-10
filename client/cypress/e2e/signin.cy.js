/* eslint-disable no-undef */

describe('login', () => {
  it('logs in successfully', () => {
    cy.visit('/')
    cy.get('#username').type('b')
    cy.get('#password').type('b')
    cy.get('button').click()
    cy.location('pathname').should('eq', '/recipes')
  })
});