/* eslint-disable no-undef */

describe("signup", () => {
  it("user already exists", () => {
    cy.visit("/");
    cy.get(".text-blue-500").click();
    cy.get("#username").type("pelle");
    cy.get("#password").type("marius");
    cy.get(".bg-green-600").click();
    cy.get(".text-red-500").should("have.text", "Username already exists");
  });
});
