/* eslint-disable no-undef */

describe("Inventory", () => {
    it("renders and adds egg to inventory", () => {
        cy.viewport(400, 800);
        cy.visit("/");
        cy.get("#username").type("b");
        cy.get("#password").type("b");
        cy.get("button").click();
        cy.get('[href="/inventory"] > .mx-auto').click();
        cy.get('select.w-full').select('Egg');
        cy.get('input.w-full').type('2');
        cy.get('.bg-blue-500').click();
});
});
