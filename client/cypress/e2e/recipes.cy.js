/* eslint-disable no-undef */

describe("recipe", () => {
  it("checks that recipes are loading correctly", () => {
    cy.visit("/");
    cy.get("#username").type("b");
    cy.get("#password").type("b");
    cy.get("button").click();
    cy.get(':nth-child(7) > .p-4 > .text-2xl').should("have.text", "Homemade Bread");  
});
});
