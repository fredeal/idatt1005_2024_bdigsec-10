# MorganMeals

Welcome to our App! This application allows users to explore various recipes, manage their shopping lists and inventory, and save their favorite recipes for later.

## Table of Contents

- [Installation](#installation)
- [Testing](#testing)
- [Usage](#usage)
- [Features](#features)
- [Authors](#authors)


## Installation

To get started with the App, follow these steps:

1. Clone the repository to your local machine:
   ```bash
   git clone https://gitlab.stud.idi.ntnu.no/fredeal/idatt1005_2024_bdigsec-10
   ```

2. Navigate to the project directory:
   ```bash
   cd idatt1005_2024_BDIGSEC-10
   ```

3. Install dependencies for both the client and server:
   ```bash
   cd client
   npm install
   cd ../server
   npm install
   ```

4. The set up for our MySQL database is already included within the repo, and we have added several example recipes and other data.


5. Start the server:
   ```bash
   npm start
   ```

6. Start the client:
   ```bash
   npm start
   ```

7. Open your browser and navigate to `http://localhost:3000` to use the App.

## Testing

To run the tests we have made for our code follow these steps:

1. To run the server-side tests:
   ```bash
   cd /server
   npm test
   ```

2. To run the client-side tests:
   ```bash
   cd /client
   npx cypress run
   ```
For the client-side you can also choose to run "npx cypress" and follow the tests being run visually.

## Usage

Once the app is running, users can:

- Browse recipes and add ingredients to their shopping list.
- View their favorite recipes and add ingredients to their shopping list.
- Manage their shopping list by adding, removing, or moving items to their inventory.
- View and manage their inventory by adding or removing items.
- Log out of their profile.

## Features

- User authentication and authorization with hashing of passwords.
- Recipe browsing and sort functionality.
- Shopping list management.
- Inventory management.
- Favorite recipe functionality.


## Authors

- Simon Leirvik Zahl ([@simonlz](https://gitlab.stud.idi.ntnu.no/simonlz))
- Frederik Andreas Lunde ([@fredeal](https://gitlab.stud.idi.ntnu.no/fredeal))
- Bjørne Morgan Øvreås ([@bjorneov](https://gitlab.stud.idi.ntnu.no/bjorneov))
- Ingrid-Margrethe Theodorsen ([@ingrimth](https://gitlab.stud.idi.ntnu.no/ingrimth))

