const request = require("supertest");
const app = require("../index");
let server;

beforeEach(() => {
  server = app.listen(8081); // Start the server before each test
});

afterEach((done) => {
  server.close(done); // Close the server after each test
});

describe("POST /signup", () => {
  it("should try to create a new user", async () => {
    const res = await request(app)
      .post("/signup")
      .send({ username: "testuser", password: "testpassword" });
    expect(res.statusCode).toEqual(409);
    expect(res.text).toEqual("Username already exists");
  });
  // testuser3 is already in database, change testuser3 to testuser4 to get test to pass
  //   it("should create a new user successfully", async () => {
  //     const res = await request(app)
  //       .post("/signup")
  //       .send({ username: "testuser3", password: "testpassword" });
  //     expect(res.statusCode).toEqual(201);
  //     expect(res.body.username).toEqual("testuser3");
  //   });
  it("should try to create a new user with an empty username", async () => {
    const res = await request(app)
      .post("/signup")
      .send({ username: "", password: "testpassword" });
    expect(res.statusCode).toEqual(409);
    expect(res.text).toEqual("Username already exists");
  });
  it("should try to create a new user with an empty password", async () => {
    const res = await request(app)
      .post("/signup")
      .send({ username: "testuser2", password: "" });
    expect(res.statusCode).toEqual(409);
    expect(res.text).toEqual("Username already exists");
  });
});

describe("POST /signin", () => {
  it("should try to sign in with the wrong password", async () => {
    const res = await request(app)
      .post("/signin")
      .send({ username: "testuser", password: "wrongpassword" });
    expect(res.statusCode).toEqual(400);
    expect(res.text).toEqual("Wrong username/password combination");
  });
  it("should try to sign in with the wrong username", async () => {
    const res = await request(app)
      .post("/signin")
      .send({ username: "wronguser", password: "testpassword" });
    expect(res.statusCode).toEqual(400);
    expect(res.text).toEqual("User does not exist");
  });
  it("should sign in successfully", async () => {
    const res = await request(app)
      .post("/signin")
      .send({ username: "testuser", password: "testpassword" });
    expect(res.statusCode).toEqual(200);
    expect(res.body.username).toEqual("testuser");
  });
  it("should try to sign in with an empty username", async () => {
    const res = await request(app)
      .post("/signin")
      .send({ username: "", password: "testpassword" });
    expect(res.statusCode).toEqual(400);
    expect(res.text).toEqual("Wrong username/password combination");
  });
  it("should try to sign in with an empty password", async () => {
    const res = await request(app)
      .post("/signin")
      .send({ username: "testuser", password: "" });
    expect(res.statusCode).toEqual(400);
    expect(res.text).toEqual("Wrong username/password combination");
  });
});

describe("GET /recipes", () => {
  it("should return all recipes", async () => {
    const res = await request(app).get("/recipes");
    expect(res.statusCode).toEqual(200);
    expect(res.body.length).toEqual(7);
  });
  it("should return all recipes of a specific type", async () => {
    const res = await request(app).get("/recipes/Baking");
    expect(res.statusCode).toEqual(200);
    expect(res.body.length).toEqual(3);
  });
});

describe("GET /recipes/ingredients", () => {
  it("should return all ingredients", async () => {
    const res = await request(app).get("/recipes/ingredients");
    expect(res.statusCode).toEqual(200);
    expect(res.body.length).toEqual(23);
  });
});

describe("GET /favorites/:username", () => {
  it("should return all favorite recipes", async () => {
    const res = await request(app).get("/favorites/b");
    expect(res.statusCode).toEqual(200);
    expect(res.body.length).toEqual(3);
  });
  it("should return all favorite recipes of a specific type", async () => {
    const res = await request(app).get("/favorites/b/Baking");
    expect(res.statusCode).toEqual(200);
    expect(res.body.length).toEqual(1);
  });
});

describe("POST /favorites", () => {
  it("should add a recipe to favorites", async () => {
    const res = await request(app)
      .post("/favorites")
      .send({ username: "b", recipeId: 4 });
    expect(res.statusCode).toEqual(400); //400 because recipe is already in favorites
  });
});

//user has to have specific recipe in favorite for test to pass
// describe("DELETE users favorite recipe", () => {
//     it("should remove a recipe from favorites", async () => {
//         const res = await request(app).delete("/favorites/b/4");
//         expect(res.statusCode).toEqual(200);
//     });
//  });

describe("Shoppinglist", () => {
    it("should get all ingredients from a specific recipe", async () => {
        const res = await request(app).get("/shoppinglist/testuser");
        expect(res.statusCode).toEqual(200);
        expect(res.body.length).toEqual(2);
    });
    it("should get ingredients in shoppinglist", async () => {
        const res = await request(app).get("/shoppinglist/testuser");
        expect(res.statusCode).toEqual(200);
        expect(res.body.length).toEqual(2);
    });
    it("should get all ingredients in inventory", async () => {
        const res = await request(app).get("/inventory/testuser");
        expect(res.statusCode).toEqual(200);
        expect(res.body.length).toEqual(2);
    });
})

describe("shoppinglist", () => {
    it("should add ingredients to shoppinglist", async () => {
        const res = await request(app)
            .post("/shoppinglist")
            .send({ username: "testuser3", ingredientId: 60, quantity: 1, unit: "tbsp" });
        expect(res.statusCode).toEqual(200);
    });
    it("should remove ingredients from shoppinglist", async () => {
        const res = await request(app).delete("/shoppinglist/44");
        expect(res.statusCode).toEqual(200);
    });
    it("should add ingredients to inventory", async () => {
        const res = await request(app)
            .post("/inventory")
            .send({ username: "testuser3", ingredientId: 60, quantity: 1, unit: "tbsp" });
        expect(res.statusCode).toEqual(200);
    });
    it("should remove ingredients from inventory", async () => {
        const res = await request(app).delete("/inventory/44");
        expect(res.statusCode).toEqual(200);
    });
});